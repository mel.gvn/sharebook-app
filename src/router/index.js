import Vue from 'vue';
import VueRouter from 'vue-router';
// import { Home, About } from '../views/Home.vue';
import Home from '../views/Home.vue';
import About from '../views/About.vue';
import AllBooks from '../views/books/AllBooks.vue';
import OneBook from '../views/books/OneBook.vue';
import OneCategory from '../views/categories/OneCategory.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
  {
    path: '/all-books',
    name: 'AllBooks',
    component: AllBooks,
  },
  {
    path: '/one-book/:id',
    name: 'OneBook',
    component: OneBook,
  },
  {
    path: '/one-category/:id',
    name: 'OneCategory',
    component: OneCategory,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
