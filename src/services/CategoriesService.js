/* eslint-disable */
import axios from 'axios';

export default class CategoriesService {
  constructor() {
    this.url = 'http://localhost:8000/sharebook-api/categories/';
  }

  async findAll() {
    const categories = await axios.get(this.url);
    return categories.data;
  }

  async findById(routeParam) {
    const category = await axios.get(this.url + routeParam);
    return category.data;
  }
}
