/* eslint-disable */
import axios from 'axios';

export default class BookService {
  constructor() {
    this.url = 'http://localhost:8000/sharebook-api/books/';
  }

  async findAll() {
    const books = await axios.get(this.url);
    return books.data;
  }

  async findAllByTitleAsc() {
    const books = await axios.get(this.url + 'title/asc');
    return books.data;
  }

  async findAllByTitleDesc() {
    const books = await axios.get(this.url + 'title/desc');
    return books.data;
  }

  async findAllByTitle() {
    const books = await axios.get(this.url + 'search-title');
    return books.data;
  }

  async findById(routeParam) {
    const book = await axios.get(this.url + routeParam);
    return book.data;
  }

  async findLastBooks() {
    const books = await axios.get(this.url + 'by-four');
    return books.data;
  }
}
