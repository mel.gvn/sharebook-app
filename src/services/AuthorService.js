/* eslint-disable */
import axios from 'axios';

export default class AuthorService {
  constructor() {
    this.url = 'http://localhost:8000/sharebook-api/authors/';
  }

  async findAll() {
    const authors = await axios.get(this.url);
    return authors.data;
  }
}
